#!/usr/bin/perl

use strict;
use warnings;

######################################################################
#
# adddnsdhcp.pl
#
######################################################################
#
# author: 	Max Voit, sdx23
# date:		12.12.2012
# license:	gpl v2
# mail:		max.voit+dvdd@with-eyes.net
#
######################################################################
#
# a simple (and maybe messy) script to add hosts already registered in a ldap
# directory the iee802-objectclass, which is needed in order of dnsdhcp to be
# capable of working with them
#
# you may as well use this script to modify ldap entries in any way required for
# dnsdhcp to work (or any other program...)
#
######################################################################

use Net::LDAP;
use Net::LDAP::Entry;
use Net::LDAP::LDIF;
use Net::LDAP::Util qw(ldap_error_desc);
use Data::Dumper;

# config
my $ldap_host = "ldap.example.com";
my $ldap_base = "dc=ldap,dc=example,dc=com";
my $ldap_user = "cn=root,".$ldap_base;
my $ldap_password = 'secret';

# try run
my $listonly = 0; 

##################################################
# CUSTOM SETTINGS PER CALL

my $searchbase = "cn=clients,cn=default,ou=nodes,".$ldap_base;
my $searchmask = "objectClass=dnsdhcpClient";

##################################################

print "Connecting...\n";

# connect to ldap
my $ldap = Net::LDAP->new($ldap_host) or die "LDAP-Verbindung fehlgeschlagen!\n";
my $mesg = $ldap->bind($ldap_user, password => $ldap_password);
if ($mesg->{'resultCode'} != 0)
{ 
   die "LDAP ERROR: " . $mesg->{'resultCode'} .": " . Net::LDAP::Util::ldap_error_text($mesg->{'resultCode'}) . "\n";
}

print "Searching...\n";

# search hosts
my $usersearch = LDAPsearch($ldap, $searchmask, 'sub', $searchbase);

my @users;
push(@users, $_->{'asn'}->{'objectName'}) for @{$usersearch->{'entries'}};


print "Processing users...\n";

for(@users){
	my $dn = $_;
    $dn =~ /cn=(.+?),/;
    my $hostname = $1;
    #print "processing user: $dn\n";
    print "$hostname\n";
    next if($listonly);
	my $result;
	$result = $ldap->modify($dn, add => { dnsdhcpPool => [ 'students' ],
										dnsdhcpOrigin => ['example.com']});
	if($result and $result->code != 20) {
	    print LDAPerror("Adding", $result);
	}
	
    #$result = $ldap->modify($dn, replace => { LConfHostgroups => "clients" });
    #if($result and $result->code) {
    #    print LDAPerror("Modifying", $result);
    #}
}


##################################################
# SUBS
##################################################

sub LDAPerror {
    my ($from, $mesg) = @_;                                                                                                                              
    print "Return code: ". 	$mesg->code;                                                                                                               
    print "\tMessage: ". 	$mesg->error_name;
    print " :".          	$mesg->error_text;
    print "MessageID: ". 	$mesg->mesg_id;
    print  "\tDN: ". 		$mesg->dn;
}


sub LDAPsearch { 
    my ($ldap,$searchString,$attrs,$base) = @_;
    #print "Base: $base\nFilter: $searchString\nAttrs: $attrs\n";
    my $result = $ldap->search ( base    => "$base",
                                 scope   => "sub",
                                 filter  => "$searchString",
                                 attrs   =>  $attrs
                              );
    return $result;
}

