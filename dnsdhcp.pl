#!/usr/bin/perl

##################################################	
#
# dnsdhcp
#
# a minimalistic approach on managing dns and dhcp 
# configuration of hosts recorded and managed 
# in a ldap directory anyways.
#
##################################################	
#
# Author: 	Max Voit, sdx23
# E-Mail: 	max.voit+dvdd@with-eyes.net
# Website: 	http://dnsdhcp.math.uni-goettingen.de
# License:	gpl v2
# Date: 	12.12.2012
# Revision: git log
#
##################################################	

=head1 NAME

dnsdhcp - generating dnsd and dhcpd configuration from ldap

=head1 DESCRIPTION

dnsdhcp is 
 a minimalistic approach on managing dns and dhcp 
 configuration of hosts recorded and managed 
 in a ldap directory anyways.

Using a template system it enables you to flexibly include hosts
information aquired from their ldap entries into configuration
files - supposedly dnsd and dhcpd configuration. 

So, in a nutshell: The script collects template-files from (possibly) remote
locations and processes them using the information grabbed from the ldap.
Afterwards the now done files are distributed to their original locations and -
if wished - services are restartet.

Access to the source is provided via the public git repository:
	git clone http://anon.git.num.math.uni-goettingen.de/dnsdhcp

=head1 USAGE

The usage is very simple. Once configured properly there's 
nothing more to do than executing this script.

=head2 Commandline options

Commandline options have precedence over the configuration. Thus you may do a
dry-run even if configured otherwise.

=head3 -n

As to be expected -n does a dry-run. This includes getting files, processing
them but B<not> distributing them to their original locations. The files in /tmp
are not deleted, so you may combine this option with later runs using I<-d> and
I<-r>.

=head3 -d

Only distribute previously generated files in /tmp.

=head3 -r

Only restart the services.

=head3 -v

Be verbose (equivalent to $debug = 1).

=head3 -h

Prints out a short help listing these options.

=cut

use strict;
use warnings;

use Net::LDAP;
use Net::LDAP::Entry;
use Net::LDAP::Util qw(ldap_error_desc);
use Template;
use Getopt::Std;
use Cwd;
use Data::Dumper;

##################################################	
# configuration
##################################################	

=head2 Configuration

The whole configuration takes - up to now - place in the script
itself. The various options are described below.

=head3 Debug Settings

	my $debug = 0;

Setting $debug to 1 will result in debug information being printet out.

=cut
my $debug = 1;

=pod

	my $dry_run = 0;

Setting $dry_run to 1 will result in the script fetching the files,
processing the templates, but B<not> distributing them back to their
hosts.

=cut
my $dry_run = 0;

=pod 

	my $config

is a hash-reference containing the main configuration described in the next two
sections.

=head4 ldap settings

	ldap_address 	=> "ldap.example.com",
	ldap_binddn 	=> "cn=nodes,dc=example,dc=com",
	ldap_bindpw 	=> "secrect",
	ldap_searchbase	=> "ou=nodes,dc=example,dc=com",

contain all settings concerning the ldap-directory to use.

=head4 general settings

The general settings influence the core behaviour of the script.

	file_list => [
			"extern:/etc/bind/master/example.com",
			"intern:/etc/dhcp3/dhcpd.conf",
		],

file_list is an array-reference. The elements are the names of the files to
be processed by this script. They may be remote locations in "scp"-style 
(host seperated via : from path) or local locations. There need to be the files
themselves on the one hand side, on the other the template-files as well. Their
name B<must> be the original filename with an attached I<.tmpl> .

	restart_services => 1,

results in the services being restarted. 
To be precise the restart_cmds are executed only when this is not set to 0.

	restart_cmds => [
			"ssh extern service bind9 force-reload",
			"ssh intern service dhcp3-server force-reload",
		],

restart_cmds is an array-reference with the commands to be executed as elements.

=cut
my $config = {

		ldap_address 	=> "ldap.example.com",
		ldap_binddn 	=> "cn=root,dc=example,dc=com",
		ldap_bindpw 	=> "secret",
		ldap_searchbase	=> "ou=nodes,dc=ldap,dc=example,dc=com",

		# not available atm.
		default_origin 	=> "",
		default_pool 	=> "",
		file_list 		=> [
				"dns-server:/etc/bind/master/bind.conf",
				"dhcp-server:/etc/dhcp3/dhcpd.conf",
			],
		restart_services	=> 1,
		restart_cmds	    => [
				"ssh dns-server 'service bind9 force-reload'",
				"ssh dhcp-server 'service dhcp3-server force-reload'",
			],
	};

=head4 ldap_map

	my $ldap_map = {

		dn 	=> 'cn',
		ip 	=> 'ipHostNumber',
		mac	=> 'macAddress',
		parent	=> 'parentNode',
		pool	=> 'dnsdhcpPool',
		origin	=> 'dnsdhcpOrigin',
		cname	=> 'dnsdhcpCname',

	};

ldap_map allows mapping ldap-attribute names to different names which may be
used in the templates. This provides some flexibility regarding already existing
structures in the ldap used. LHS is the internally (and template-side) used
identifier, RHS the ldap attribute name.

=cut
my $ldap_map = {

    	dn 		=> 'cn',
    	ip 		=> 'ipHostNumber',
    	mac		=> 'macAddress',
    	parent	=> 'parentNode',
    	pool	=> 'dnsdhcpPool',
    	origin	=> 'dnsdhcpOrigin',
    	cname	=> 'dnsdhcpCname',

	};

=head3 Templating

As template engine the Template-Toolkit (L<http://template-toolkit.com>) is used.
The hosts themselves are organized in an array called I<hostlist>. All of them
have the attributes defined in I<$ldap_map> which contain the values stored in
the ldap.

Some simple examples for dhcp, dns and rdns follow below.

=head4 dhcpd.conf

A very simple approach would be the following (only in the context of a group,
in the format of isc-dhcp3):

  group {
    option routers 134.76.80.111; 

	# some hosts not in ldap (for some reasons)
    host testhost1 { hardware ethernet 00:15:4f:40:29:8c; fixed-address test1.example.com;  }
    host testhost2 { hardware ethernet 00:15:4f:40:ab:a0; fixed-address test2.example.com;  }

	# all ldap hosts to follow
    [% FOREACH host = hostlist %]
    host [% host.name %] { hardware ethernet [% host.mac %]; fixed-address [% host.ip %]; }
    [% END %]

  }

Now you may want to distinguish certain groups of hosts and place them in
different groups or pools in the dhcpd.conf as well. Just use their attributes to
do so, possibly:

    [% FOREACH host = hostlist %]
        [% IF host.pool == "mitarbeiter" %]
    host [% host.name %] { hardware ethernet [% host.mac %]; fixed-address [% host.ip %]; }
        [% END %]
    [% END %]

=head4 dnsd.conf

In the dnsd.conf you can do just the same, e.g.:

	$ORIGIN example.com
	; some hosts not in ldap
	test1		A	10.0.2.1
	testera			CNAME	test1
	test2		A	10.0.2.2
	testerb			CNAME	test2
	; clients
	[% FOREACH host = hostlist %]
	    [% IF (host.parent == "desktops")  %]
	
	[% host.name %]         A   [% host.ip %]
	        [% FOREACH cname = host.cname %]
	
	[% cname %]     CNAME   [% host.name %]
	        [% END %]
	    [% END %]
	[% END %]

Take care of that emty lines - without them it will get messed up. Also, note
that the I<cname> attribute is an array and thus to be iterated over.

Concerning reverse-dns there's the I<reverseip> attribute:

	$ORIGIN in-addr.arpa.
	[% FOREACH host = hostlist %]
	    [% IF host.pool == "studenten"  %]
	
	[% host.reverseip %]         PTR   [% host.dn %].
	    [% END %]
	[% END %]

=cut 

##################################################	
# main
##################################################	

=head1 INTERNALS

You may obtain some information about the internal workings of dnsdhcp below.

=head2 Program flow

=over 4

=item * check cli options

=item * collect_hosts

=item * get_files

=item * insert_hosts

=item * distribute_files

=back

=cut

#check_config - not necessary atm. 

# check cli options: -n -d -r and -v -h
our ($opt_n, $opt_d, $opt_r, $opt_v, $opt_h);
getopts('ndrvh');

print_help_only() if $opt_h;

# verbose and debug=1 is the same
$debug = 1 if $opt_v;

# do everything when called without options
unless($opt_n or $opt_d or $opt_r or $dry_run){
	print "Neither options nor dry_run found, do ALL the things!\n" if $debug;
	$opt_n = $opt_d = 1;
	$opt_r = $config->{'restart_services'};
}

# change dir stuff
my $saved_cwd = cwd();
mkdir '/tmp/dnsdhcp' unless( -d '/tmp/dnsdhcp');
chdir '/tmp/dnsdhcp'; 

if($opt_n or $dry_run){
	# collect all host data from ldap
	my $hosts = collect_hosts();
	
	# aquire dnsd and dhcpd configs and templates, check them
	get_files();
	
	# insert collected hosts into files
	insert_hosts($hosts);
}

if($opt_d){
	# distribute files to new locations
	distribute_files();
}

if($opt_r){
	# restart services(dns and dhcp daemons) if necessary
	restart_services();
}

chdir($saved_cwd);

##################################################	
# subs
##################################################	

=head2 Subroutines

=head3 collect_hosts()

Collects host information from ldap 
and reformats it into a nice datastructure.

=cut 
sub collect_hosts {
	# connect/bind to ldap
	print "Connecting to LDAP...\n" if $debug;

	my $ldap = Net::LDAP->new($config->{'ldap_address'})
		or die "LDAP-connection failed!\n";

	my $mesg = $ldap->bind( $config->{'ldap_binddn'},
							password => $config->{'ldap_bindpw'} );

	if ($mesg->{'resultCode'} != 0) { 
		die "LDAP ERROR: "  
			. $mesg->{'resultCode'} . ": "
			. Net::LDAP::Util::ldap_error_text($mesg->{'resultCode'}) . "\n";
	}

	# search hosts
	print "Searching...\n" if $debug;

	my $result = $ldap->search(
								base 	=> $config->{'ldap_searchbase'},
								scope 	=> 'sub',
								attrs	=> ['cn', 
											'ipHostnumber',
											'macAddress',
											'parentNode',
											'dnsdhcpOrigin',
											'dnsdhcpCname',
											'dnsdhcpPool'],
								filter 	=> 'objectClass=dnsdhcpClient' 
							);
	print "\tFound ".$result->count." hosts.\n" if $debug;

	# reformat information
	print "Aquiring client information...\n" if $debug;

	my $hosts;
	for( my $i = 0; $i < $result->count; $i++) {
		my $entry = $result->entry($i); 
		my $hostinfo;

		$hostinfo->{'dn'} = $entry->get_value($ldap_map->{'dn'});

		my $hostname = $hostinfo->{'dn'};
		$hostname =~ s/\..+//;

		$hostinfo->{'name'} = $hostname;
		$hostinfo->{'ip'} = $entry->get_value($ldap_map->{'ip'});
		$hostinfo->{'mac'} = $entry->get_value($ldap_map->{'mac'});
		$hostinfo->{'parent'} = $entry->get_value($ldap_map->{'parent'});
		$hostinfo->{'pool'} = $entry->get_value($ldap_map->{'pool'});
		$hostinfo->{'origin'} = $entry->get_value($ldap_map->{'origin'});
		$hostinfo->{'cname'} = $entry->get_value($ldap_map->{'cname'}, asref => 1);
		$hostinfo->{'reverseip'} = reverseip($hostinfo->{'ip'});

		#$hosts->{$hostname} = $hostinfo;
		push(@$hosts, $hostinfo) if ($hostinfo->{'ip'} && $hostinfo->{'mac'} && $hostinfo->{'name'});
	}

	# sorted by hostnames
	my @sorted_hosts = sort {lc($a->{'name'}) cmp lc($b->{'name'})} @$hosts;

	return \@sorted_hosts;
}

=head3 get_files()

Collects dnsd and dhcpd configuration file from (possibly remote) hosts.

=cut
sub get_files {
	# get files via scp
	# non templates not needed right now
	print "Fetching files...\n" if $debug;
	foreach my $file (@{$config->{'file_list'}}) {
		my $file_lt = fn_localtmpl($file);

		print "\t$file.tmpl \t ->\t$file_lt\n" if $debug;

		`scp $file.tmpl $file_lt`
	;#		or die "Couldn't fetch file $file: $?\n";
	}
}

=head3 insert_hosts()

Processes templatefiles with information collected.

=cut
sub insert_hosts {
	my $hosts = shift;

	print "Processing templates...\n" if $debug;
	# create template engine
	my $tmpl_config = {
			INCLUDE_PATH 	=> '',
			INTERPOLATE 	=> 0,
			POST_CHOMP 		=> 0,
			PRE_CHOMP 		=> 1,
			EVAL_PERL 		=> 1,
			TRIM 			=> 0,
		};
	my $template = Template->new($tmpl_config);

	# process files
	my $vars = {
			hostlist => $hosts,
		};

	foreach my $file (@{$config->{'file_list'}}) {
		my $file_lt = fn_localtmpl($file);
		my $file_l = fn_local($file);

		print "\t$file_lt \t ->\t$file_l\n" if $debug;

		$template->process($file_lt, $vars, $file_l) 
			or die $template->error();
	}
}

=head3 distribute_files()

Distributes the configurations files back to their locations.

=cut
sub distribute_files {
	print "Distributing files...\n" if $debug;

	foreach my $file (@{$config->{'file_list'}}) {
		my $file_l = fn_local($file);

		print "\t$file_l \t ->\t$file\n" if $debug;

		`scp $file_l $file`
	;#		or die "Couldn't distribute file $file: $?\n";
	}
}

=head3 fn_localtmpl($filename)

Helper function. Takes a filename, returns the local-template-filename.

=cut
sub fn_localtmpl {
	my $file = shift;

	return fn_local($file).".tmpl";
}

=head3 fn_local($filename)

Helper function. Takes a filename, returns the local-filename.

=cut
sub fn_local {
	my $file = shift;

	my $file_l = $file;
	$file_l =~ s/[\/\:]/_/g;

	return $file_l;
}

=head3 reverseip($ip)

Helper function. Takes an ip, returns the reversed ip.

=cut
sub reverseip {
	my $ip = shift;

	$ip =~ /(\d+)\.(\d+)\.(\d+)\.(\d+)/;

	return "$4.$3.$2.$1";
}

=head3 restart_services()

Restarts dns and dhcp deamons.

=cut
sub restart_services {
	print "Restarting services...\n" if $debug;
	for my $cmd (@{$config->{'restart_cmds'}}){
		print "\t$cmd\n" if $debug;
		`$cmd`;
	}
}

=head3 print_help_only()

Prints some help and exits gracefully.

=cut
sub print_help_only {
	print <<EOF;
dnsdhcp - version (???)

dnsdhcp is a script aquiring, processing and distributing template files for
dnsd and dhcpd configuration whilest collecting information from a ldap.

Use: dnsdhcp [-vndr]

Possible command line switches are:
	-v 			be verbose about what you do
	-n 			dry run - only get files and process them, no distributing
	-d 			distribute files only (not sensible without previous -n -run)
	-r 			restart services only
	-h 			this help...

EOF
	exit 0;
}

